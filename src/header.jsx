import React from "react";

function Header() {
  return (
    <>
      <div className="w-1/1 pt-3 h-20 bg-gradient-to-r from-purple-500 to-pink-500 flex justify-center">
        <h1 className="text-4xl text-white">People Of GOT 👑</h1>
      </div>
    </>
  );
}

export default Header;
