import React from "react";
import { useState } from "react";
import Cards from "./cards";

function Button(props) {
  let copy = props.buttonName;
  const [state, setState] = useState(copy);

  const filter = (e) => {
    for (let index = 0; index < copy.length; index++) {
      if (e.target.innerHTML == copy[index].name.toUpperCase()) {
        setState([copy[index]]);
      }
    }

    let name = e.target.parentElement;
    let allbtn = name.querySelectorAll("button");
    for (let index = 0; index < allbtn.length; index++) {
      allbtn[index].setAttribute("index", index);

      if (e.target.attributes.index == allbtn[index].attributes.index) {
        allbtn[index].className =
          "bg-white mt-4 ml-2 p-2 text-black border-solid border-2 border-black";
      } else {
        allbtn[index].className = "bg-black mt-4 ml-2 p-2 text-white rounded";
      }
    }

    // console.log(name);
  };

  function handleClick(e) {
    if (e.target.value == "") {
      setState(copy);
    } else {
      let searchValue = e.target.value;
      let clone = JSON.stringify(copy);
      clone = JSON.parse(clone);
      let dataArray = clone.map((family) => {
        family.people = family.people.filter((person) => {
          return person.name.toLowerCase().includes(searchValue.toLowerCase());
        });
        return family;
      });
      setState(dataArray);
    }
  }

  return (
    <>
      <div className="w-1/1 h-20 bg-gradient-to-r from-purple-500 to-pink-500 flex justify-center">
        <input
          className="w-1/4 h-7 mt-3 px-1 rounded"
          placeholder="Search Got Family"
          onKeyUp={handleClick}
        />
      </div>
      <div className="w-1/1 flex justify-center flex-wrap">
        {props.buttonName.map((element) => {
          return (
            <button
              className=" bg-black mt-4 ml-2 p-2 text-white rounded "
              key={element.name}
              value={element.name}
              onClick={filter}
            >
              {element.name.toUpperCase()}
            </button>
          );
        })}
      </div>
      <Cards info={state} />
    </>
  );
}

export default Button;
