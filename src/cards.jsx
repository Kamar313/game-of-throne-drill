import React from "react";

function Cards(props) {
  return (
    <>
      <section className="flex mt-16 flex-wrap justify-center">
        {props.info.map((cardsInfo) => {
          return cardsInfo.people.map((peopleInfo) => {
            return (
              <div
                className="m-2 bg-teal-500/50 w-96 h-56 flex flex-col items-center relative rounded"
                key={peopleInfo.name}
              >
                <img
                  className="rounded-full h-16 mt-2"
                  src={peopleInfo.image}
                  alt={peopleInfo.name}
                />
                <h2 className="text-xl font-bold mt-2">{peopleInfo.name}</h2>
                <p className="pl-3">{peopleInfo.description}</p>
                <button className="bg-black h-10 text-white p-2 absolute bottom-1 rounded">
                  <a href={peopleInfo.wikiLink}>Know More?</a>
                </button>
              </div>
            );
          });
        })}
      </section>
    </>
  );
}

export default Cards;
