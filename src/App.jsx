import Header from "./header";
import Button from "./button";
import data from "./data_2";
function App() {
  return (
    <>
      <Header />
      <Button buttonName={data.houses} />
    </>
  );
}

export default App;
